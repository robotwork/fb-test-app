#include <unistd.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/fb.h>

#include "common.h"
#include "sync_cache.h"

#define FBIO_CACHE_SYNC         0x4630
void sync_cache(int fb_fd, unsigned char *mem_start)
{
    if (fb_fd < 0)
        return;
    const unsigned int pitch = 4 * 854;
    const unsigned int y = 0;
    const unsigned int h = 480;
    const unsigned int w = 854;

    unsigned int args[2];
    unsigned int dirty_rect_vir_addr_begin = (unsigned int) (mem_start);
    unsigned int dirty_rect_vir_addr_end  = (unsigned int) (mem_start + pitch * (y + h));
    args[0] = dirty_rect_vir_addr_begin;
    args[1] = dirty_rect_vir_addr_end;
    // fprintf(stderr, "pitch=%d w=%d h=%d memstart=%p begin=%p end=%p\n", pitch, w, h, mem_start, dirty_rect_vir_addr_begin, dirty_rect_vir_addr_end);
    ioctl(fb_fd, FBIO_CACHE_SYNC, args);
    // printf("synced\n");
}

/* seed msg to fb driver to call pan_disp */
void video_sync(int fb_fd, unsigned char *mem_start)
{
    struct fb_var_screeninfo var;
    ioctl(fb_fd, FBIOGET_VSCREENINFO, &var);

    sync_cache(fb_fd, mem_start);

    var.yoffset = 0;
    var.reserved[0] = 0;
    var.reserved[1] = 0;
    var.reserved[2] = 854;
    var.reserved[3] = 480;
    ioctl(fb_fd, FBIOPAN_DISPLAY, &var);
}